mod help;
use reqwest;
use opencv::{
    core::{Point, Vector},
    imgcodecs,
    imgproc,
    prelude::*,
    videoio,
};
use std::{
    ffi::{
        CString,
        c_void
    },
    fs::File,
    io::Write,
    os::unix::io::FromRawFd,
    thread::sleep,
    time::{Duration, Instant},
};


fn run() -> opencv::Result<()> {
    let mut cam = videoio::VideoCapture::new(0, videoio::CAP_ANY)?;  // 0 is the default camera
    if ! videoio::VideoCapture::is_opened(&cam)? {
        panic!("Unable to open source 0 camera!");
    }

    let width:  usize = 640;
    let height: usize = 480;
    let mut dev_fd = unsafe {
        let fd = help::open_sink_device(CString::new("/dev/video9")?.as_ptr(), width as u64, height as u64);
        if fd < 0 {
            panic!("Unable to open 9 camera!");
        }
        File::from_raw_fd(fd)
    };

    let mut bg_vid = videoio::VideoCapture::from_file("/tmp/bg_aurora_loop.mkv", videoio::CAP_ANY)?;
    let mut buf = vec![0 as u8; height * width * 2];
    loop {
        let loop_beg_time = Instant::now();

        let mut orig_frame = Mat::default()?;
        cam.read(&mut orig_frame)?;
        if orig_frame.size()?.width <= 0 {
            break;
        }

        let mut jpg = Vector::<u8>::new();
        imgcodecs::imencode(".JPG", &orig_frame, &mut jpg, &Vector::<i32>::new())?;
        let mask_bytes = reqwest::blocking::Client::new()
            .post("http://localhost:9000")
            .header("Content-Type", "application/octet-stream")
            .body(jpg.to_vec())
            .send().expect("...!?")
            .bytes()
            .unwrap();
        let fg_mask = unsafe {
            Mat::new_rows_cols_with_data(
                height as i32, width as i32,
                opencv::core::CV_8U,
                mask_bytes.as_ptr() as *mut c_void,
                opencv::core::Mat_AUTO_STEP)?
        };
        let fg_mask = fg_mask.mul(&255., 1.0)?.to_mat()?;
        let mut new_fg_mask = Mat::default()?;
        imgproc::blur(&fg_mask, &mut new_fg_mask, opencv::core::Size::new(20, 20), Point::new(-1, -1), opencv::core::BORDER_DEFAULT)?;
        let mut bg_mask = Mat::default()?;
        opencv::core::bitwise_not(&new_fg_mask, &mut bg_mask, &opencv::core::no_array()?)?;

        let mut bg_vid_frame = Mat::default()?;
        bg_vid.read(&mut bg_vid_frame)?;
        if bg_vid_frame.empty()? {
            bg_vid.set(videoio::CAP_PROP_POS_FRAMES, 0.)?;
            bg_vid.read(&mut bg_vid_frame)?;
        }

        let mut new_bg_vid_frame = Mat::default()?;
        imgproc::resize(&bg_vid_frame, &mut new_bg_vid_frame, opencv::core::Size::new(width as i32, height as i32), 0., 0., imgproc::INTER_LINEAR)?;
        let mut bg_frame = Mat::default()?;
        opencv::core::bitwise_or(&new_bg_vid_frame, &new_bg_vid_frame, &mut bg_frame, &bg_mask)?;

        let mut fg_frame = Mat::default()?;
        opencv::core::bitwise_or(&orig_frame, &orig_frame, &mut fg_frame, &new_fg_mask)?;
        let mut frame = Mat::default()?;
        opencv::core::bitwise_or(&fg_frame, &bg_frame, &mut frame, &opencv::core::no_array()?)?;

        let mut yuv_frame = Mat::default()?;
        imgproc::cvt_color(&frame, &mut yuv_frame, imgproc::COLOR_BGR2YUV, 0)?;
        // println!("{:?}", yuv_frame);

        let num_ch = 3;
        let elm_sz = 1;
        let pix_sz = num_ch * elm_sz;
        let src_buf = unsafe {
            std::slice::from_raw_parts(yuv_frame.data()?, width * height * pix_sz)
        };
        for h in 0..height {
            let row_beg = h * width * 2;
            for w in 0..width {
                buf[row_beg + w * 2] = src_buf[h * width * pix_sz + w * pix_sz];
            }
            for w in 0..width/2 {
                buf[row_beg + w * 4 + 1] = src_buf[h * width * pix_sz + w * 2 * pix_sz + elm_sz];
                buf[row_beg + w * 4 + 3] = src_buf[h * width * pix_sz + w * 2 * pix_sz + elm_sz * 2];
            }
        }
        dev_fd.write(&buf[..]).unwrap();

        let frame_duration = loop_beg_time.elapsed();
        let it = 33 - frame_duration.as_millis() as i128;
        if it > 1 {
            sleep(Duration::new(0, 1_000_000 * it as u32));
        }
    }

    Ok(())
}

fn main() {
    run().unwrap();
}
