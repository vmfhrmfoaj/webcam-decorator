#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/videodev2.h>
#include "help.h"

int open_sink_device(const char* dev_path, unsigned long width, unsigned long height) {
    struct v4l2_format dev_fmt;
    int dev_fd = open(dev_path, O_RDWR | O_SYNC);
    if (dev_fd < 0) {
        return -1;
    }

    /**/
    dev_fmt.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
    if (ioctl(dev_fd, VIDIOC_G_FMT, &dev_fmt) == -1){
		return -1;
	}

    dev_fmt.fmt.pix.width  = width;
	dev_fmt.fmt.pix.height = height;
	dev_fmt.fmt.pix.bytesperline = width * 2;
	dev_fmt.fmt.pix.sizeimage    = width * height * 2;
	dev_fmt.fmt.pix.colorspace = V4L2_COLORSPACE_JPEG;
	dev_fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
	dev_fmt.fmt.pix.field = V4L2_FIELD_NONE;
	if (ioctl(dev_fd, VIDIOC_S_FMT, &dev_fmt) == -1){
		return -1;
	}

    return dev_fd;
}
