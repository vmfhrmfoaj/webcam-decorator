use std::env;
use std::path::PathBuf;

fn main() {
    println!("cargo:rerun-if-changed=src/help.c");
    println!("cargo:rerun-if-changed=src/help.h");
    println!("cargo:rustc-link-lib=help");
    cc::Build::new()
        .file("src/help.c")
        .compile("help");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindgen::Builder::default()
        .header("src/help.h")
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .generate()
        .expect("Unable to generate bindings")
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}
